package org.example;

import org.junit.jupiter.api.Test;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TestRegexMatcher {
    @Test
    void shouldMatch() {
        final String regex = "CN=(.*?)(?:,|$)";
        final Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        final String value = "1.2.840.113549.1.9.1=#160f7661737961406465762e6c6f63616c,CN=vasya,OU=Security,O=Student,C=RU";
        final Matcher matcher = pattern.matcher(value);
        assertTrue(matcher.find());
        final int groups = matcher.groupCount();
        assertEquals(1, groups);
        final String group = matcher.group(1);
        assertEquals("vasya", group);
    }
}
