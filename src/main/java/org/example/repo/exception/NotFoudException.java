package org.example.repo.exception;

public class NotFoudException extends RuntimeException {
  public NotFoudException() {
  }

  public NotFoudException(String message) {
    super(message);
  }

  public NotFoudException(String message, Throwable cause) {
    super(message, cause);
  }

  public NotFoudException(Throwable cause) {
    super(cause);
  }

  public NotFoudException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
