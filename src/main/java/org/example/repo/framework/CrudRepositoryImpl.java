package org.example.repo.framework;

import org.example.framework.di.annotation.Component;
import org.example.repo.framework.exception.NoIdException;
import org.example.repo.framework.exception.ReflectionException;

import javax.persistence.Id;
import java.lang.reflect.Field;
import java.util.*;
@Component

class CrudRepositoryImpl<ENTITY, ID> implements CrudRepository<ENTITY, ID> {
  private final JpaTransactionTemplate template;
  private final Class<ENTITY> entityClazz;
  private final Field id;

  public CrudRepositoryImpl(final JpaTransactionTemplate template, final Class<ENTITY> entityClazz) {
    this.template = template;
    this.entityClazz = entityClazz;
    this.id = Arrays.stream(entityClazz.getDeclaredFields())
        .filter(o -> o.isAnnotationPresent(Id.class))
        .findFirst()
        .orElseThrow(NoIdException::new);

    init();
  }

  private void init() {
    id.setAccessible(true);
  }

  @Override
  public List<ENTITY> getAll(final int limit, final int offset) {
    return template.executeInTransaction(em -> em.createQuery(
                "SELECT e FROM " + entityClazz.getSimpleName() + " e WHERE e.removed=false",
                entityClazz
            )
            .setFirstResult(limit) // offset
            .setMaxResults(offset) // limit
            .getResultList()
    );
  }

  @Override
  public Optional<ENTITY> getById(ID id) {
    return template.executeInTransaction(em -> Optional.ofNullable(em.find(entityClazz, id)));
  }

  @Override
  public ENTITY save(ENTITY entity) {
    return template.executeInTransaction(em -> {
      try {
        final Object id = this.id.get(entity);
        if (id == null) {
          em.persist(entity);
          return entity;
        }
        return em.merge(entity);
      } catch (IllegalAccessException e) {
        throw new ReflectionException(e);
      }
    });
  }

  @Override
  public ENTITY getByLogin(final String login) {
    return template.executeInTransaction(em -> em.createQuery(
                            "SELECT e FROM " + entityClazz.getSimpleName() + " e WHERE e.login=:login",
                            entityClazz
                    )
            .setParameter("login",login).getSingleResult());
  }


  @Override
  public List<ENTITY> saveAll(final Iterable<ENTITY> entities) {
    return template.executeInTransaction(em -> {
      final List<ENTITY> result = new ArrayList<>();
      final Iterator<ENTITY> iterator = entities.iterator();
      while (iterator.hasNext()) {
        final ENTITY entity = iterator.next();
        final ENTITY saved = save(entity);
        result.add(saved);
      }
      return Collections.unmodifiableList(result);
    });
  }

}
