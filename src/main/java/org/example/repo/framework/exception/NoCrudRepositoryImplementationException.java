package org.example.repo.framework.exception;

public class NoCrudRepositoryImplementationException extends RuntimeException {
  public NoCrudRepositoryImplementationException() {
  }

  public NoCrudRepositoryImplementationException(String message) {
    super(message);
  }

  public NoCrudRepositoryImplementationException(String message, Throwable cause) {
    super(message, cause);
  }

  public NoCrudRepositoryImplementationException(Throwable cause) {
    super(cause);
  }

  public NoCrudRepositoryImplementationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
