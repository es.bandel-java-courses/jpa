package org.example.repo.framework.exception;

public class JpaTransactionException extends RuntimeException {
  public JpaTransactionException() {
  }

  public JpaTransactionException(String message) {
    super(message);
  }

  public JpaTransactionException(String message, Throwable cause) {
    super(message, cause);
  }

  public JpaTransactionException(Throwable cause) {
    super(cause);
  }

  public JpaTransactionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
