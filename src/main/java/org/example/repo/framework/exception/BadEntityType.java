package org.example.repo.framework.exception;

public class BadEntityType extends RuntimeException {
  public BadEntityType() {
  }

  public BadEntityType(String message) {
    super(message);
  }

  public BadEntityType(String message, Throwable cause) {
    super(message, cause);
  }

  public BadEntityType(Throwable cause) {
    super(cause);
  }

  public BadEntityType(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
