package org.example.repo.framework;

import lombok.RequiredArgsConstructor;
import org.example.repo.framework.exception.JpaTransactionException;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

@RequiredArgsConstructor
class JpaTransactionTemplate {
  private final EntityManagerFactory emf;

  public <T> T executeInTransaction(final Callback<T> callback) {
    EntityManager em = null;
    EntityTransaction tx = null;

    try {
      em = emf.createEntityManager();
      tx = em.getTransaction();
      tx.begin();
      final T result = callback.execute(em);
      tx.commit();

      return result;
    } catch (Exception e) {
      if (tx != null) {
        tx.rollback();
      }
      throw new JpaTransactionException(e);
    } finally {
      if (em != null) {
        em.close();
      }
    }
  }
}
