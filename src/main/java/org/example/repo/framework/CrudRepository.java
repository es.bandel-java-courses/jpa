package org.example.repo.framework;

import java.util.List;
import java.util.Optional;

public interface CrudRepository<ENTITY, ID> {
  List<ENTITY> getAll(int limit, int offset);
  Optional<ENTITY> getById(ID id);
  ENTITY save(ENTITY entity);
  List<ENTITY> saveAll(Iterable<ENTITY> entities);
  ENTITY getByLogin(final String login);
}
