package org.example.repo.framework;

import io.github.classgraph.ClassGraph;
import io.github.classgraph.ClassInfo;
import io.github.classgraph.ClassInfoList;
import io.github.classgraph.ScanResult;
import org.example.repo.exception.NotFoudException;
import org.example.repo.framework.exception.BadEntityType;
import org.example.repo.framework.exception.InvalidTypeParameterCount;
import org.example.repo.framework.exception.NoCrudRepositoryImplementationException;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RepositoryCreator {
  private static final Class<CrudRepository> BASE_CLAZZ = CrudRepository.class;
  private final String persistenceUnitName;
  private final EntityManagerFactory emf;
  private final JpaTransactionTemplate template;
  private final List<CrudRepository<?, ?>> repositories = new ArrayList<>();

  public RepositoryCreator(final String persistenceUnitName) {
    this.persistenceUnitName = persistenceUnitName;
    this.emf = Persistence.createEntityManagerFactory(this.persistenceUnitName);
    this.template = new JpaTransactionTemplate(emf);
  }

  public void scan(final String basePackage) {
    final ClassGraph classGraph = new ClassGraph();
    try (
        final ScanResult scanResult = classGraph.acceptPackages(basePackage).scan();
    ) {
      final ClassInfoList classInfoList = scanResult.getClassesImplementing(BASE_CLAZZ);
      for (final ClassInfo classInfo : classInfoList) {
        final Class<?> clazz = classInfo.loadClass();
        final Object repository = create(clazz);
        repositories.add((CrudRepository<?, ?>) repository);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public <T> T provideBean(final Class<T> repoClazz) {
    return (T) repositories.stream()
        .filter(o -> Arrays.stream(o.getClass().getInterfaces()).anyMatch(i -> i.isAssignableFrom(repoClazz)))
        .findFirst()
        .orElseThrow(NotFoudException::new);
  }

  private <T> T create(final Class<T> repoClazz) {
    final ParameterizedType parameterizedType = Arrays.stream(repoClazz.getGenericInterfaces())
        .map(ParameterizedType.class::cast)
        .filter(o -> BASE_CLAZZ.equals(o.getRawType()))
        .findFirst()
        .orElseThrow(NoCrudRepositoryImplementationException::new);

    final Type[] typeArguments = parameterizedType.getActualTypeArguments();
    if (typeArguments.length != 2) {
      throw new InvalidTypeParameterCount();
    }

    final Type entityType = typeArguments[0];
    if (!(entityType instanceof Class)) {
      throw new BadEntityType("should be Class");
    }

    final Class<Object> entityClazz = (Class<Object>) entityType;

    final CrudRepositoryImpl<Object, Object> repo = new CrudRepositoryImpl<>(template, entityClazz);

    return (T) Proxy.newProxyInstance(
        repoClazz.getClassLoader(),
        new Class[]{repoClazz}, (proxy, method, args) -> method.invoke(repo, args)
    );
  }
}
