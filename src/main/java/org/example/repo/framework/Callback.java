package org.example.repo.framework;

import javax.persistence.EntityManager;

interface Callback<T> {
  T execute(final EntityManager em);
}
